<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Filters
Route::get('filters', [\App\Http\Controllers\FiltersController::class, 'index']);
Route::get('filters/create', [\App\Http\Controllers\FiltersController::class, 'create']);
Route::post('filters', [\App\Http\Controllers\FiltersController::class, 'store']);
Route::get('filters/{id}', [\App\Http\Controllers\FiltersController::class, 'show']);
Route::get('filters/{id}/edit', [\App\Http\Controllers\FiltersController::class, 'edit']);
Route::put('filters/{id}', [\App\Http\Controllers\FiltersController::class, 'update']);
Route::delete('filters/{id}', [\App\Http\Controllers\FiltersController::class, 'destroy']);
