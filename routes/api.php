<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Events
Route::post('webhook', [\App\Http\Controllers\WebhookController::class, 'store']);

//Tools
Route::get('tools', [\App\Http\Controllers\ToolsController::class, 'index']);

//Filters
Route::get('filters', [\App\Http\Controllers\FiltersController::class, 'index']);
Route::post('filters', [\App\Http\Controllers\FiltersController::class, 'store']);

//Clients
Route::get('clients', [\App\Http\Controllers\ClientsController::class, 'index']);
