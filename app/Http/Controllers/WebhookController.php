<?php

namespace App\Http\Controllers;

use App\Models\Filters;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filters = Filters::all('json');
        $filters = html_entity_decode($filters[0]->json, true);
        $filters = json_decode($filters, true);

        $allTasks = new Collection();

        $tasks = collect($filters['filtros']);

        return $tasks;

        foreach ($tasks as $task) {
            $allTasks->push(([
                $task['field'] ?? null => $task['value'] ?? null,
                'operator' => $task['operator'] ?? null,
            ]));
        }

        $filtered = $allTasks->where('', '', '');

        return $allTasks;

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Filters $filters
     * @return \Illuminate\Http\Response
     */
    public
    function show(Filters $filters)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Filters $filters
     * @return \Illuminate\Http\Response
     */
    public
    function edit(Filters $filters)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Filters $filters
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, Filters $filters)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Filters $filters
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(Filters $filters)
    {
        //
    }
}
