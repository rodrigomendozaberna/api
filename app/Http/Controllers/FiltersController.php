<?php

namespace App\Http\Controllers;

use App\Models\Filters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class FiltersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = Filters::all();
        return View::make('home.index')->with('filters', [$filters]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filters = Filters::create([
            'name' => $request->name,
            'json' => json_encode($request->json),
        ]);

        return response()->json('Add Success');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Filters $filters
     * @return \Illuminate\Http\Response
     */
    public function show(Filters $filters)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Filters $filters
     * @return \Illuminate\Http\Response
     */
    public function edit(Filters $filters)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Filters $filters
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Filters $filters)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Filters $filters
     * @return \Illuminate\Http\Response
     */
    public function destroy(Filters $filters)
    {
        //
    }
}
